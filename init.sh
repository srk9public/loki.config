#AS ROOT: '$ sudo -i'

echo '*************************************************************'
echo '*************************************************************'
echo '*******  Stage 1 of 3 - Locking down firewall   *********'
echo '*************************************************************'
echo '*************************************************************'

ufw enable
ufw default allow outgoing
ufw default deny incoming
ufw allow http
ufw allow https
ufw allow from 10.0.0.0/24 to any port 22
ufw allow from 192.168.0.0/24 to any port 22

echo '*************************************************************'
echo '*************************************************************'
echo '*******  Stage 2 of 3 - Updating system   *******************'
echo '*************************************************************'
echo '*************************************************************'

cp /etc/apt/sources.list /etc/apt/sources.list.bak
wget -qO- https://gitlab.com/srk9public/loki.config/raw/master/resources/configs/sources.list.config > /etc/apt/sources.list
apt-get update && apt-get dist-upgrade -qy

echo '*************************************************************'
echo '*************************************************************'
echo '*******  Stage 3 of 3 - Updating system   *******************'
echo '*************************************************************'
echo '*************************************************************'

export DEBIAN_FRONTEND=noninteractive


sudo apt-get update
# Fix any broken sources in "/etc/apt/sources.list.d/"
#sudo apt-get dist-upgrade -qy
# Some developer basic tools
sudo apt-get install -qy build-essential checkinstall software-properties-common software-properties-gtk gdebi elementary-sdk #snapd
# Basic python stuff
sudo apt-get install -qy python-dev python-software-properties python-setuptools python-pip #snapcraft
# Additional dev tools
sudo apt-get install -qy vim nginx gnome-system-monitor htop ppa-purge unattended-upgrades gnome-nettool
# Install gparted & diverse partition support
sudo apt-get install -qy gparted gnome-disk-utility exfat-utils exfat-fuse hfsplus hfsutils hfsprogs
# Install backup software (http://blog.domenech.org/2013/01/backing-up-ubuntu-using-deja-dup-backup-and-aws-s3.html)
sudo apt-get install -qy deja-dup duplicity python-boto python-cloudfiles dconf-tools rcconf
# Install browsers
sudo apt-get install -qy firefox
# Install specialty software
sudo apt-get install -qy gimp imagemagick exiftool hugin gtk2-engines-pixbuf libffi-dev
# Misc. Software
sudo apt-get install -qy blender libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev
# Graphics Card Tests
sudo apt-get install -qy glmark2 mesa-utils
# Extras
sudo apt-get install -qy pithos

sudo apt-get install -qy openssh-server

sudo apt-get install -qy zsh


# Cleanup
apt-get autoremove -qy
apt-get clean -qy

git clone https://gitlab.com/srk9public/loki.config.git

echo '*************************************************************'
echo '*************************************************************'
echo '********************   COMPLETE   ***************************'
echo '*************************************************************'
echo '*************************************************************'
