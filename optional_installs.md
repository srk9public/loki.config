Google Chrome

    sh -c "$(curl -fsSL https://gitlab.com/srk9public/loki.config/raw/master/resources/scripts/chrome_stable.sh)";

Atom

    sh -c "$(curl -fsSL https://gitlab.com/srk9public/loki.config/raw/master/resources/scripts/atom.sh)";

Docker

    sh -c "$(curl -fsSL https://gitlab.com/srk9public/loki.config/raw/master/resources/scripts/docker.sh)";
    
WakeOnLan

    sh -c "$(curl -fsSL https://gitlab.com/srk9public/loki.config/raw/master/resources/scripts/wake_on_lan.sh)";

Bluetooth PAM

    sh -c "$(curl -fsSL https://gitlab.com/srk9public/loki.config/raw/master/resources/scripts/bluetooth_authentication.sh)";

Elemamentary Tweaks

    sh -c "$(curl -fsSL https://gitlab.com/srk9public/loki.config/raw/master/resources/scripts/elementary_tweaks.sh)";

##REQUIRE USER ATTENTION FOR INSTALL:

Avahi

    sh -c "$(curl -fsSL https://gitlab.com/srk9public/loki.config/raw/master/resources/scripts/avahi.sh)";
    
    sudo -i && cat /usr/share/doc/avahi-daemon/examples/ssh.service > /etc/avahi/services/ssh.service && exit;
    
DDClient

    sh -c "$(curl -fsSL https://gitlab.com/srk9public/loki.config/raw/master/resources/scripts/ddclient.sh)";
    
Tripwire

    sh -c "$(curl -fsSL https://gitlab.com/srk9public/loki.config/raw/master/resources/scripts/tripwire.sh)";
