#### Optional - Don't ask for password all the time (for automation)

This step will allow you to use the sudo command without entering the password each time. This will ease the rest of the setup.

Copy the text outputed from the following command.

    echo "Copy this --> echo '$USER ALL=NOPASSWD:ALL' > /etc/sudoers.d/nopass"

Now type:

    sudo -i

Enter your password.

Paste and press enter.

Type: 

    exit


#### Breaks Automation!!!

    sudo apt-get install -y ubuntu-restricted-extras

#### Hostname

    sudo vi /etc/hostname

    sudo vi /etc/hosts

#### Disks

Get UUID's

    blkid

Auto Mount Disks

    sudo vi /etc/fstab

#### VIM

    WHERE DID YOU GO???

#### GIT

    git config --global user.email "$USEREMAIL"

    git config --global user.name "$USERNAME"

#### AWS (http://docs.aws.amazon.com/codecommit/latest/userguide/setting-up-ssh-unixes.html)

    touch ~/.ssh/config && chmod 600 ~/.ssh/config

Upload new key to AWS (https://console.aws.amazon.com/iam/home?region=us-east-1)

    Host git-codecommit.*.amazonaws.com
    User ALKJLKJLKJUYUTUUT
    IdentityFile ~/.ssh/codecommit_rsa`

#### gCloud (https://cloud.google.com/storage/docs/gsutil_install)

    curl https://sdk.cloud.google.com | sudo bash
    exec -l $SHELL
    gcloud init

#### CUPS (https://help.ubuntu.com/12.04/serverguide/cups.html)

    sudo usermod -aG lpadmin $USER
    xdg-open http://127.0.0.1:631

#### Plank

    plank --preferences

#### Docker

    docker run -it ubuntu bash

#### ZSH

    zsh --version (https://github.com/robbyrussell/oh-my-zsh/wiki/Installing-ZSH)
    chsh -s $(which zsh)

#### OHMYZSH (https://github.com/robbyrussell/oh-my-zsh)

    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Install plugins and themes (https://github.com/powerline/fonts)

# Install zsh plugins (https://github.com/robbyrussell/oh-my-zsh/wiki/External-plugins)

# Install Netatalk

sudo apt install build-essential -qy
sudo apt install libevent-dev -qy
sudo apt install libssl-dev -qy
sudo apt install libgcrypt-dev -qy
sudo apt install libkrb5-dev -qy
sudo apt install libpam0g-dev -qy
sudo apt install libwrap0-dev -qy
sudo apt install libdb-dev -qy
sudo apt install libtdb-dev -qy
sudo apt install libmysqlclient-dev -qy
sudo apt install avahi-daemon -qy
sudo apt install libavahi-client-dev -qy
sudo apt install libacl1-dev -qy
sudo apt install libldap2-dev -qy
sudo apt install libcrack2-dev -qy
sudo apt install systemtap-sdt-dev -qy
sudo apt install libdbus-1-dev -qy
sudo apt install libdbus-glib-1-dev -qy
sudo apt install libglib2.0-dev -qy
sudo apt install libio-socket-inet6-perl -qy
sudo apt install tracker -qy
sudo apt install libtracker-sparql-1.0-dev -qy
sudo apt install libtracker-miner-1.0-dev -qy

echo 'Done Installing Netatalk dependencies'

#Download and unzip netatalk

tar xvf netatalk-3.1.11.tar.bz2
cd netatalk-3.1.11


./configure --with-init-style=debian-systemd --without-libevent --without-tdb --with-cracklib --enable-krbV-uam --with-pam-confdir=/etc/pam.d --with-dbus-daemon --with-dbus-sysconf-dir=/etc/dbus-1/system.d --with-tracker-pkgconfig-version=1.0

make
make install



