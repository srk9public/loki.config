# BETA STATUS - USE AT YOUR OWN RISK
================================================

INSTRUCTIONS: 
------------

<!-- REFERENCE: https://itsfoss.com/things-to-do-after-installing-elementary-os-loki/ -->

The purpose of this project is assist in the rapid setup and automated configuration of an immediatly usable develompent machine. Keep this window open as you work thru the following steps.

### Prerequisits: 

<!--Elementary OS Loki (16.04 Xenial): https://goo.gl/A26Gkl -->

You must first install [Elementary OS](https://elementary.io/) LOKI on your machine and have sudo privliges.


Step 1: Install - Init (Lockdown firewall && upgrade system && install some default software packages via apt-get install)

    sh -c "$(curl -fsSL https://gitlab.com/srk9public/loki.config/raw/master/init.sh)";
    
    
Step 2: Exit into regular user mode (exit out of root)

    exit

Step 3: Install Oh-my-zsh (requires password)
    
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)";


Step 4: Init ZSH 

<!-- sudo chsh -s $(which zsh) -->

    wget -qO- http://gitlab.com/srk9public/loki.config/raw/master/resources/configs/zshrc.config > ~/.zshrc && source .zshrc;
    
Step 5: Install Secondary Package Managers and Packages

    installSecondaryPackageManagers && reload && nvm install 6.11.1 && nvm alias default 6.11.1 && nvm use default && installSecondaryPackages

Step 6: Install Default Settings (gsettings / dconf)

    installCustomConfigs


