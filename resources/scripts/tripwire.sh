#The Tripwire binaries are located in /usr/sbin and the database is located in /var/lib/tripwire. 
#It is strongly advised that these locations be stored on write-protected   │ 
#media (e.g. mounted RO floppy). See /usr/share/doc/tripwire/README.Debian for details.

sudo apt-get install -qy tripwire

#https://gist.github.com/Jiab77/ffd5164cc170bbadd9a7ece776c50852
#https://www.server-world.info/en/note?os=Ubuntu_16.04&p=tripwire
#https://www.unixmen.com/install-tripwire-intrusion-det-such-file-or-directory/