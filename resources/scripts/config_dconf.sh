#!/usr/bin/zsh

export DEBIAN_FRONTEND=noninteractive

echo "#################################################################"
echo "#################################################################"
echo "#################################################################"
echo "####################      STARTING      #########################"
echo "#################################################################"
echo "#################################################################"
echo "#################################################################"

# echo "Enabaling 'CTRL+ALL+BACKSPACE' in order to kill 'hung X sessions'"
gsettings set org.gnome.desktop.input-sources xkb-options "['terminate:ctrl_alt_bksp']"
# echo "Requiring 'Double-Click' to open files and disabaling 'Single-Click' functionality"
gsettings set org.pantheon.files.preferences single-click false
dconf write /org/pantheon/terminal/settings/unsafe-paste-alert false

echo "#################################################################"
echo "#################################################################"
echo "#################################################################"
echo "####################      COMPLETE     ##########################"
echo "#################################################################"
echo "#################################################################"
echo "#################################################################"


echo "Next time before shutting down or restarting this machine, please test CTRL+ALT+BACKSPACE to make sure it kills X (incase you get locked up someday)"