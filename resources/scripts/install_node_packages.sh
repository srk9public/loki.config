#!/usr/bin/zsh

export DEBIAN_FRONTEND=noninteractive

echo "#################################################################"
echo "#################################################################"
echo "#################################################################"
echo "####################      STARTING      #########################"
echo "#################################################################"
echo "#################################################################"
echo "#################################################################"


#nvm install 6.11.1 && nvm alias default 6.11.1 && nvm use default;

# Install commonly used global node modules
npm install -g node-gyp
npm install -g shx #https://github.com/shelljs/shx
npm install -g trash-cli
npm install -g bower
npm install -g node-dev
npm install -g coffee-script cson
npm install -g jade stylus 
npm install -g onchange #https://github.com/Qard/onchange
npm install -g nodemon #https://github.com/remy/nodemon/
npm install -g ember-cli 
npm install -g firebase-tools
npm install -g polymer-cli
npm install -g yo generator-webapp generator-electron

# Install commonly used global gems
#gem install jekyll
#gem install bundler




#Install Atom (https://github.com/atom/atom)

echo "#################################################################"
echo "#################################################################"
echo "#################################################################"
echo "####################      COMPLETE     ##########################"
echo "#################################################################"
echo "#################################################################"
echo "#################################################################"
