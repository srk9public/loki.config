#!/usr/bin/zsh

#wget -qO- https://gitlab.com/srk9public/loki.config/raw/master/etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive

echo "#################################################################"
echo "#################################################################"
echo "#################################################################"
echo "####################      STARTING      #########################"
echo "#################################################################"
echo "#################################################################"
echo "#################################################################"

sudo apt-get update
# Fix any broken sources in "/etc/apt/sources.list.d/"
#sudo apt-get dist-upgrade -qy
# Some developer basic tools
sudo apt-get install -qy build-essential checkinstall software-properties-common software-properties-gtk gdebi elementary-sdk #snapd
# Basic python stuff
sudo apt-get install -qy python-dev python-software-properties python-setuptools python-pip #snapcraft
# Additional dev tools
sudo apt-get install -qy vim nginx gnome-system-monitor htop ppa-purge unattended-upgrades gnome-nettool
# Install gparted & diverse partition support
sudo apt-get install -qy gparted gnome-disk-utility exfat-utils exfat-fuse hfsplus hfsutils hfsprogs
# Install backup software (http://blog.domenech.org/2013/01/backing-up-ubuntu-using-deja-dup-backup-and-aws-s3.html)
sudo apt-get install -qy deja-dup duplicity python-boto python-cloudfiles dconf-tools rcconf
# Install browsers
sudo apt-get install -qy firefox
# Install specialty software
sudo apt-get install -qy gimp imagemagick exiftool hugin gtk2-engines-pixbuf libffi-dev
# Misc. Software
sudo apt-get install -qy blender libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev
# Graphics Card Tests
sudo apt-get install -qy glmark2 mesa-utils
# Extras
sudo apt-get install -qy pithos

sudo apt-get install -qy openssh-server


# Cleanup
apt-get autoremove -qy
apt-get clean -qy

echo "#################################################################"
echo "#################################################################"
echo "#################################################################"
echo "####################      COMPLETE     ##########################"
echo "#################################################################"
echo "#################################################################"
echo "#################################################################"    
