#!/bin/bash

# Docker  (https://docs.docker.com/engine/installation/linux/ubuntulinux/)
sudo apt-get install -qy apt-transport-https ca-certificates linux-image-extra-$(uname -r) linux-image-extra-virtual
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo add-apt-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
sudo apt-get update
sudo apt purge lxc-docker
sudo apt-cache policy docker-engine
sudo apt install -qy docker-engine
sudo systemctl start docker
sudo docker run hello-world


