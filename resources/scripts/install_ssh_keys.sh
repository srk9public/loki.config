#!/bin/bash
FILES=../../../ssh_keys/*
AUTHORIZED_KEYS_FILE=/keys/ssh/$USER/authorized_keys
for f in $FILES
do
  cat $f >> $AUTHORIZED_KEYS_FILE
done

cat $AUTHORIZED_KEYS_FILE
