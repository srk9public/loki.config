#!/usr/bin/zsh

export DEBIAN_FRONTEND=noninteractive

echo "#################################################################"
echo "#################################################################"
echo "#################################################################"
echo "####################      STARTING      #########################"
echo "#################################################################"
echo "#################################################################"
echo "#################################################################"

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.29.0/install.sh | sh #bash

#install yarn
# curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
# echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
# sudo apt-get update && sudo apt-get install yarn -y

# Install RVM and Ruby (Stable)
#gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
#\curl -sSL https://get.rvm.io | bash -s stable --ruby
#sudo source /etc/profile.d/rvm.sh
#source /home/dustin/.rvm/scripts/rvm

#sudo source /etc/profile.d/rvm.sh

echo "#################################################################"
echo "#################################################################"
echo "#################################################################"
echo "####################      COMPLETE     ##########################"
echo "#################################################################"
echo "#################################################################"
echo "#################################################################"
