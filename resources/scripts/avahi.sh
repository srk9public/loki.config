#Configure Firewall
sudo ufw allow 5353/udp

#https://wiki.ubuntu.com/ZeroConfNetworking
sudo apt-get install -qy avahi-daemon avahi-discover avahi-utils #libnss-mdns mdns-scan

#sudo cat /usr/share/doc/avahi-daemon/examples/ssh.service > /etc/avahi/services/ssh.service

#configure a discoverable sftp service
#sudo wget -qO- https://gitlab.com/srk9public/loki.config/raw/master/resources/services/sftp.service > /etc/avahi/services/sftp.service

sudo systemctl restart avahi-daemon.service